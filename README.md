# API sample for the Pervasive Computing course, ISI UNIBO, a.y. 2017-2018. #

DoctorBed tries to reinvent the Hospital beds, in a way that makes it possible for every patient to be monitored and "cured" by the very bed they are lying onto.

This bed will have sensors which are capable to connect to doctor's device, giving basic and static information about the patient, such as their identification information and their past medical history (past treatments, deseases, heart conditions and such).

The patient will be directly attached to the bed, instead of being attached to an external monitoring system, so that the bed will also know the patient's current life parameters, and it will also dispense the right amount of drugs (which are going to be held inside of it in specific containers) as stated by the treatements decided by the doctor in charge.

The API will make it possible to:

- ask the bed for a real-time checkup of all of its sensors
- ask the bed for a real-time checkup of all of its drug levels
- get the patient's ID information 
- get the patient's medical history
- get and create/modify treatements issued by doctors, and make the bead handle the treatment
- get and update the patient's life parameters in real-time, as perceived by each sensor
- get notified when a sensor perceives a life threatening value for a specific life paramter, and list every out-of-range life parameter in real time

Here's a list of each possible request in detail: https://swaggerhub.com/apis/doctorbed/doctorbed2/1.0.0

Version 1.0.0
alberto.giunta@studio.unibo.it
michele.campobasso2@studio.unibo.it